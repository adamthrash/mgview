import sys
import os

class Node:
	value = None
	out_edges = []
	in_edge = None
	name = ""
	rank = ""
	size = 0

	def __init__(self, value, in_edge, rank, name):
		self.value = value
		self.out_edges = []
		self.in_edge = in_edge
		self.name = name
		self.rank = rank
		self.size = 0

	def add_edge(self, edge_type, target):
		if edge_type == "out":
			if target not in self.out_edges:
				self.out_edges.append(target)
		elif edge_type == "in":
			self.in_edge = target

	def remove_edge(self, edge_type, target):
		if edge_type == "out":
			if target in self.out_edges:
				self.out_edges.remove(target)
		elif edge_type == "in":
			if target == self.in_edge:
				self.in_edge = None

	def child_exists(self, target):
		return (target in self.out_edges)

	def parent_exists(self, target):
		return (target in self.in_edge)

	def set_edge(self, edge_type, old_target, new_target):
		self.remove_edge(edge_type, old_target)
		self.add_edge(edge_type, new_target)

class Graph:

	vertex_list = {}

	def __init__(self):
		self.vertex_list = {}

	def add_vertex(self, vertex_value, in_edge, rank, name, children):
		temp_vertex = Node(vertex_value, in_edge, rank, name)
		for child in children:
			temp_vertex.add_edge("out", child)
		self.vertex_list[vertex_value] = temp_vertex

	def add_edge(self, source, target):
		self.vertex_list[source].add_edge("out", target)
		self.vertex_list[target].add_edge("in", source)

	def remove_edge(self, source, target, weight):
		self.vertex_list[source].remove_edge("out", target)
		self.vertex_list[target].remove_edge("in", source)

	def collapse_vertex(self, vertex_value):
		removed_vertex = self.vertex_list[vertex_value]
		del self.vertex_list[vertex_value]
		for out_edge in removed_vertex.out_edges:
			recipient_vertex = self.vertex_list[out_edge]
			recipient_vertex.set_edge("in", removed_vertex.value, removed_vertex.in_edge)
			self.vertex_list[removed_vertex.in_edge].add_edge(recipient_vertex.value)

	def print_vertices(self):
		for each in self.vertex_list:
			vertex = self.vertex_list[each]
			print(vertex.value)
			print(vertex.out_edges)
			print(vertex.in_edge)
			print(vertex.name)
			print(vertex.rank)
			print("----")

	def print_vertex(self, vertex_value):
		vertex = self.vertex_list[vertex_value]
		print(vertex.value)
		print(vertex.out_edges)
		print(vertex.in_edge)
		print(vertex.name)
		print(vertex.rank)
		print("----")
	def get_edge(self, source, target):
		vertex = self.vertex_list[source]
		return vertex.get_edge(target)

	def get_parent(self, vertex_value):
		return self.vertex_list[vertex_value].in_edge

	def get_children(self, vertex_value):
		return self.vertex_list[vertex_value].out_edges

	def number_of_vertices(self):
		return len(self.vertex_list)

	def edge_list(self):
		edges = []
		for vertex_value in self.vertex_list:
			vertex = self.vertex_list[vertex_value]
			for edge in vertex.out_edges:
				for weight in vertex.out_edges[edge]:
					edges.append((vertex_value,edge,weight, "out"))
			for edge in vertex.in_edges:
				for weight in vertex.in_edges[edge]:
					edges.append((vertex_value,edge,weight, "in"))
		return edges

	def depth_first_search(self, start_value):
		visited = []
		queue = []
		start_vertex = self.vertex_list[start_value]
		visited.append(start_vertex.value)
		print("{\"name\": "+str(start_value)+", \"children\": [")
		for target in start_vertex.out_edges:
			queue.append(target)
		while queue != []:
			target_value = queue.pop()
			if target_value not in visited:
				visited.append(target_value)
				if target != None:
					queue.append(target)
		return visited

	def recursive_depth_first_search(self, start_value, visited):
		annotated_lineage = ["superkingdom", "kingdom", "phylum", "class", "order", "suborder", "family", "genus", "species"]
		start_vertex = self.vertex_list[start_value]
		visited.append(start_vertex.value)
		# sys.stderr.write(start_vertex.name)
		if start_vertex.size != 0:
			if start_vertex.rank in annotated_lineage or start_value == 1:
				sys.stdout.write("{\"name\": \""+start_vertex.name+"\",")
				sys.stdout.write("\"size\": "+str(start_vertex.size)+", \"children\": [")
				for target in start_vertex.out_edges:
					if target == None and start_vertex.size != 0:
						sys.stdout.write("{\"name\": \""+start_vertex.name+"\", \"size\": "+str(start_vertex.size)+"},")
					if target not in visited and target != None:
						visited = self.recursive_depth_first_search(target, visited)
				sys.stdout.write("],")
				sys.stdout.write("},")
			else:
				for target in start_vertex.out_edges:
					if target == None and start_vertex.size != 0:
						sys.stdout.write("{\"name\": \""+start_vertex.name+"\", \"size\": "+str(start_vertex.size)+"},")
					if target not in visited and target != None:
						visited = self.recursive_depth_first_search(target, visited)
		return visited
			

	def reverse_depth_first_search(self, start_value):
		visited = []
		queue = []
		start_vertex = self.vertex_list[start_value]
		visited.append(start_vertex.value)
		queue.append(start_vertex.in_edge)
		while queue != []:
			target_value = queue.pop()
			if target_value not in visited:
				visited.append(target_value)
				queue.append(self.vertex_list[target_value].in_edge)
		return visited

	def breadth_first_search(self, start_value):
		visited = []
		queue = []
		start_vertex = self.vertex_list[start_value]
		visited.append(start_vertex.value)
		for target in start_vertex.out_edges:
			queue.append(target)
		while queue != []:
			target_value = queue.pop(0)
			if target_value not in visited:
				visited.append(target_value)
				for target in self.vertex_list[target_value].out_edges:
					queue.append(target)
		return visited





g = Graph()

with open(sys.argv[1], 'r') as taxa_file:
	for line in taxa_file:
		data = line.split("|=:=|")
		children = data[4].strip("\n")[1:-1]
		if len(children) > 1:
			if len(children.split(", ")) == 1:
				if children != "''":
					int_children = [int(children[1:-1])]
				else:
					int_children = []
			else:
				int_children = [int(child[1:-1]) for child in children.split(", ")]
			g.add_vertex(int(data[0]), int(data[1]), data[2], data[3], int_children)
		else:
			g.add_vertex(int(data[0]), int(data[1]), data[2], data[3], [None])

deleted = []
with open('deleted.dat', 'r') as deleted_file:
	for line in deleted_file:
		deleted.append(int(line.strip("\n")))

with open(sys.argv[2], 'r') as data_file:
	for line in data_file:
		if len(line.strip("\n")) > 1:
			data = line.split("\t")
			taxa = data[1].split(", ")
			if len(taxa) > 1:
				sys.stderr.write("Multiple hits.\n")
				parents = []
				for each in taxa:
					counts = each.split(" ")
					if counts[0] != "N/A":
						taxon = int(counts[0])
						parents.append(taxon)
				while len(parents) > 1:
					# sys.stderr.write("Searching for common ancestor: "+str(parents)+"\n")
					vertex = g.vertex_list[parents.pop(0)]
					if vertex.in_edge not in parents:
						parents.append(vertex.in_edge)
				taxa = parents
			for each in taxa:
				counts = each.split(" ")
				if len(counts[0].split(";")) > 1:
					split_taxa = counts[0].split(";")
					parents = []
					for each in split_taxa:
						if each != "N/A":
							parents.append(int(each))
					while len(parents) > 1:
						# sys.stderr.write("Searching for common ancestor: "+str(parents)+"\n")
						vertex = g.vertex_list[parents.pop(0)]
						if vertex.in_edge not in parents:
							parents.append(vertex.in_edge)
						# print(parents)
					taxon = parents[0]
				else:
					if counts[0] != "N/A":
						taxon = int(counts[0])
				if taxon not in deleted:
					for vertex in g.reverse_depth_first_search(taxon):
						g.vertex_list[vertex].size+=1
# to_delete = []
# for value in g.vertex_list:
# 	vertex = g.vertex_list[value]
# 	if len(vertex.out_edges) == 1 and vertex.out_edges[0] != None and vertex.size > 0:
# 		parent = g.vertex_list[vertex.in_edge]
# 		child = g.vertex_list[vertex.out_edges[0]]
# 		parent.add_edge("out", child.value)
# 		parent.name = parent.name+"."+vertex.name
# 		child.in_edge = parent.value
# 		g.vertex_list[vertex.in_edge] = parent
#                 g.remove_edge(parent.value, vertex.value, 1)
#                 g.remove_edge(child.value, vertex.value, 1)
#                 g.vertex_list[vertex.out_edges[0]] = child
# 		to_delete.append(value)

# for value in to_delete:	
# 	del g.vertex_list[value]
sys.stderr.write("Searching...\n")
g.recursive_depth_first_search(1, [])
