adjacency_list = {}
ranks = {}
parents = {}

with open("nodes.dmp", 'r') as nodes_file:
	for line in nodes_file:
		data = line.strip("\n").split("\t|\t")[0:3]
		taxon = data[0]
		parent = data[1]
		rank = data[2]

		if taxon not in adjacency_list:
			adjacency_list[taxon] = []

		if parent in adjacency_list:
			adjacency_list[parent].append(taxon)
		else:
			adjacency_list[parent] = [taxon]

		ranks[taxon] = rank
		parents[taxon] = parent
merged_nodes = {}
with open("merged.dmp", 'r') as merged_file:
	for line in merged_file:
		data = line.strip("\t|\n").split("\t|\t")
		old = data[0]
		new = data[1]
		merged_nodes[old] = new
		
names = {}
with open("names.dmp", 'r') as names_file:
	for line in names_file:
		if "scientific" in line:
			data = line.strip("\t|\n").split("\t|\t")
			taxon = data[0]
			name = data[1]
			names[taxon] = name

with open("taxa.dat", 'w') as taxa_data:
	for taxon in adjacency_list:
		taxa_data.write(taxon+"|=:=|"+parents[taxon]+"|=:=|"+ranks[taxon]+"|=:=|"+names[taxon]+"|=:=|"+str(adjacency_list[taxon])+"\n")